const axios = require("axios");
const { mapperCategories, mapperProducts } = require("../tools/dataMapper");

const author = {
    author: {
        "name": "Juan Manuel",
        "lastname": "Espalza Mora"
    }
}

const products = async (req, res) => {
    console.log("products");
    await axios.get("https://api.mercadolibre.com/sites/MLA/search?q=" + req.query.q)
        .then((response) => {
            const body = {
                ...author,
                categories: mapperCategories(response.data.filters),
                items: mapperProducts(response.data.results.slice(0, 4))
            }
            console.log(body);
            res.json(body)
        }).catch(err=>console.log(err))

}

const product = async (req, res) => {
    console.log("product");
    const product = await axios.get(`https://api.mercadolibre.com/items/${req.params.id}`);
    const description = await axios.get(`https://api.mercadolibre.com/items/${req.params.id}/description`)

    Promise.all([product, description])
        .then(
            ([pro, des]) => {
                const body = {
                    ...author,
                    item: {
                        ...mapperProducts([pro.data])[0],
                        sold_quantity: pro.data.sold_quantity,
                        description: des.data,
                        picture: pro.data.pictures
                    }
                }
                console.log(body);
                res.json(body)
            }
        ).catch((err) => res.json(err))

}

module.exports = { products, product }