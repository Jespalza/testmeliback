exports.mapperCategories = (filters) => {
    return filters.length >= 1
        ? filters
            .find((cat) => cat.id === "category")
            .values[0]
            .path_from_root
            .map((value) => value.name)
        : null;
}

exports.mapperProducts = (products) => {
    return infoProduct(products)
}

infoProduct = (products) => {
    let _products = []
    products.forEach(product => {
        _products.push(
            {
                id: product.id,
                title: product.title,
                price: {
                    currency: product.currency_id,
                    amount: product.price
                },
                picture: product.thumbnail,
                condition: product.condition,
                free_shipping: product.shipping?.free_shipping,
                address: product.address,
            }
        )

    });
    return _products
}