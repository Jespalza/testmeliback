const { Router } = require("express");
const { products, product } = require("../controllers/searchsML.controller");
const router = Router();

router.get('/', products)
router.get('/:id', product)

module.exports = router;