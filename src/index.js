const express = require("express");
var cors = require("cors")
const app = express()
const port = 3002

app.use(cors())
app.use("/api/items/", require('./routers/index.routes'))
app.listen(port)

console.log("Server run port: " + port)