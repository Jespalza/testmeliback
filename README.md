# Backend Test MeLi
El presente proyecto es para suplir las necesidades establecidas en la prueba técnica enviada por el equipo de MELI.

## Requerimiento
1. Se requiere un servidor implementado en NodeJS y Express con 2 endPoint.
2. Se requiere un endPoint para ser usado al momento de generar las búsquedas de los productos, dicho endpoint es un método GET el cual recibe un parámetro en la url mediante Query con e item a buscar: “/api/items?q=:query”
3. Se requiere un endPoint para ser usado al momento de ingresar a la informacion del producto, este endPoint consume 2 métodos get externos donde en los cuales consulta la información y descripción del producto

#### Iniciar Proyecto
### `node src/index.js` o `npm start`

#### Instalar Modulos
### `npm i` o `npm install`
